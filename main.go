package main

import (
	"encoding/csv"
	"fmt"
	"log"
	"os"

	"github.com/PuerkitoBio/goquery"
	flag "github.com/spf13/pflag"
	"golang.org/x/text/unicode/norm"
)

type Circle struct {
	space string
	name  string
	url   string
}

func (c Circle) Space() string {
	return c.space
}

func (c Circle) Name() string {
	return c.name
}

func (c Circle) URL() string {
	return c.url
}

type CircleList struct {
	circles []Circle
}

func (cl CircleList) Circles() []Circle {
	return cl.circles
}

type Event struct {
	eventName string
	eventTimes uint64
	circleList CircleList
}

func (e Event) EventName() string {
	return e.eventName
}

func (e Event) EventTimes() uint64 {
	return e.eventTimes
}

func (e Event) CircleList() CircleList {
	return e.circleList
}

var (
	times = flag.Uint64P("times", "t", 130, "number of times held comitia.")
)

const (
	comitia115URL = "https://www.comitia.co.jp/history/%dlist_sp.html"
)

func DisplayCircleListWithCSVFormat(e Event) {
	writer := csv.NewWriter(os.Stdout)
	writer.UseCRLF = true
	for _, circle := range e.circleList.Circles() {
		writer.Write([]string{circle.Space(), circle.Name(), circle.URL()})
	}
	writer.Flush()
}

var DisplayFuncs = map[string]func(e Event) {
	"csv": DisplayCircleListWithCSVFormat,
}

func ParseComitia115(doc *goquery.Document) (CircleList, error) {
	var results []Circle

	doc.Find("table").Find("tr").Each(func(index int, selection *goquery.Selection) {
		tags := selection.Find("td")
		spaceElem := tags.First()
		nameElem := tags.First().Next()

		// Skip index row
		if _, isIndex := spaceElem.Attr("colspan"); isIndex {
			return
		}

		space := norm.NFKC.String(spaceElem.Text())

		// Skip null row
		if space == "" {
			return
		}

		name := nameElem.Text()

		if url, urlExists := nameElem.Find("a").Attr("href"); urlExists {
			results = append(results, Circle{space, name, url})
		} else {
			results = append(results, Circle{space, name, ""})
		}
	})

	return CircleList{results}, nil
}

var Parsers = map[string]func(*goquery.Document) (CircleList, error){
	"ParseComitia115": ParseComitia115,
}

func main() {
	flag.Parse()

	url := fmt.Sprintf(comitia115URL, *times)
	doc, err := goquery.NewDocument(url)
	if err != nil {
		log.Fatal(err)
	}

	circleList, err := Parsers["ParseComitia115"](doc)
	if err != nil{
		log.Fatal(err)
	}

	event := Event{"Comitia", *times, circleList}

	DisplayFuncs["csv"](event)
}
