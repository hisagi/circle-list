module circle-list

go 1.13

require (
	github.com/PuerkitoBio/goquery v1.5.0
	github.com/spf13/pflag v1.0.5
	golang.org/x/text v0.3.2
)
