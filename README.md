# circle-list

This is the repository for managing the non-official circle lists of Comitia.

The format of the lists is 'yaml' and 'csv'.

# How to make your own circle list

## Requirements

* docker

The documents how to install is here: https://docs.docker.com/install/

## How to build

```
docker build -t circle-list .
```

## How to run

```
docker run circle-list
```
