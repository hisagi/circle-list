FROM golang:1 AS build
ADD . /work
WORKDIR /work
RUN go get -d github.com/PuerkitoBio/goquery && \
    go get -d golang.org/x/text/unicode/norm && \
    go get -d github.com/spf13/pflag && \
    CGO_ENABLED=0 go build -o circle-list ./main.go

FROM golang:1
COPY --from=build /work/circle-list /usr/local/bin/circle-list
ENTRYPOINT ["/usr/local/bin/circle-list"]